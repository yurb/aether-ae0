AE = new function () {
    var self = this;
    this.config = {
        connections: {
            neutral: "rgba(255, 255, 255, 0.2)",
            selected: "#ae0"
        },
    }

    this.popup = d3.select('#video-popup');

    this.initPoetry = function () {
        var container = d3.select("#poetry");
        YAML.load("data/poetry.yaml", (poetry) => {
            this.poetry = poetry;

            /* Poems */
            poems = container
                .selectAll('section')
                .data(poetry)
                .enter()
                .append('section')
                .classed('poem', true)
                .classed('author-i', (d, i) => d.author == "i")
                .classed('author-m', (d, i) => d.author == "m")
                .attr("id", d => d.id);

            poems
                .append('div')
                .classed('text', true)
                .html(d => d.poem);

            /* Links */
            poems
                .append('div')
                .classed('nexts', true)
                .selectAll('a.next')
                .data(d => d.next || 0)
                .enter()
                .append('a')
                .classed('next', true)
                .attr('href', d => "#" + d)
                .text((d, i) => i);

            /* Operations */
            var ops = poems
                .append('div')
                .classed('operations', true);

            ops
                .append('button')
                .attr('id', 'reverse')
                .text('R')
                .on('click', function () {
                    var elem = d3.select(this.parentNode.parentNode).select('.text');
                    self.reverseText(elem);
                });

            ops.each(function (d) {
                var elem = d3.select(this);
                if (elem.datum().video) {
                    for (var direction in elem.datum().video) {
                        var label = direction == 'forward' ? 'f' : 'b';
                        var file = elem.datum().video[direction];
                        elem
                            .append('button')
                            .classed('.open-video', true)
                            .text('V/' + label)
                            .attr('data-file', file)
                            .on('click', function () {
                                var file = d3.select(this).attr('data-file');
                                var author = elem.datum().author;
                                self.openVideo(file, author);
                            });
                    }
                }
                if (elem.datum().audio) {
                    for (var lang in elem.datum().audio) {
                        var label = lang;
                        var file = elem.datum().audio[lang];
                        elem
                            .append('button')
                            .classed('open-audio', true)
                            .text('A/' + label)
                            .attr('data-file', file)
                            .on('click', function () {
                                var file = d3.select(this).attr('data-file');
                                var author = elem.datum().author;
                                self.openAudio(file, author);
                            });
                    }
                }
            });

            d3.select('#video-popup .close')
                .on('click', self.closeVideo);

            /* Graph */
            var graph = d3.select("#graph");
            var nodes = graph.selectAll("circle")
                .data(poetry)
                .enter()
                .append("g");

            nodes
                .append("circle")
                .attr("r", 5);

            nodes
                .append("text")
                .classed("first-word", true)
                .text(d => d.poem.replace(/(<([^>]+)>)/,"").split(" ").shift());

            var connections = [];
            poetry.forEach(row => {
                if (row.next) {
                    row.next.forEach(next => {
                        connections.push({source: row.id, target: next});
                    });
                }
            });
            var links = graph.selectAll("line")
                .data(connections)
                .enter()
                .append("line")
                .attr("stroke", this.config.connections.neutral);

            /* Forces */
            var sim = d3.forceSimulation()
                .force("link", d3.forceLink().id(row => row.id))
                .force("repulsion", d3.forceCollide().radius(25))
                .force("center", d3.forceCenter(500, 300))
                .velocityDecay(0.05)
                .alphaTarget(1)
                .stop();

            sim.nodes(poetry);
            sim.force("link")
                .links(connections)
                .strength((d) => d.active ? 0.1 : 0.02)
                .distance((d) => d.active ? 100 : 250);

            var ticked = function () {
                links
                    .attr("x1", d => d.source.x)
                    .attr("y1", d => d.source.y)
                    .attr("x2", d => d.target.x)
                    .attr("y2", d => d.target.y);

                nodes
                    .attr("transform", d => `translate(${d.x} ${d.y})`);
            }
            sim.on("tick", ticked);

            var run_sim = function () {
                sim.tick();
                ticked();
                requestAnimationFrame(run_sim);
            }
            requestAnimationFrame(run_sim);

            /* Moving through links */
            poems.each(function () {
                var parent = d3.select(this);
                var source = parent.datum().id;
                parent.selectAll('a.next').on('click', function () {
                    var next = d3.select(this).attr('href').slice(1);
                    var link = links.filter((d) =>
                                            d.target.id == next &&
                                            d.source.id == source);
                    link.attr("stroke", self.config.connections.selected);
                    var datum = link.datum();
                    datum.active = true;
                    sim.force("link").links(connections);
                    sim.restart();
                });
            });
        });
    }

    this.initCamera = function () {
        var target = document.getElementById('camera');
        navigator.mediaDevices
            .getUserMedia({video: true, audio: false})
            .then(function (stream) {
                target.srcObject = stream;
                target.play();
            });
    }

    this.reverseText = function (elem) {
        var reversed = elem.text().split('').reverse().join('').trim();
        elem.text(reversed);
    }

    this.openPopup = function (author) {
        this.popup.classed('active', true);
        this.popup.classed(`author-${author}`, true);
        this.popup.style('top', Math.floor(Math.random() * 30 + 7) + '%');
        this.popup.style('left', Math.floor(Math.random() * 30 + 7) + '%');
    }

    this.openVideo = function (file, author) {
        this.popup
            .append('video')
            .attr('controls', true)
            .attr('src', `media/poems/${file}`)
            .node().play();
        this.openPopup(author);
    }

    this.openAudio = function (file, author) {
        this.popup
            .append('audio')
            .attr('controls', true)
            .attr('src', `media/poems/${file}`)
            .node().play();
        this.openPopup(author);
    }

    this.closeVideo = function () {
        self.popup.classed('active', false);
        self.popup.selectAll('video, audio').remove();
    }

    this.initCamera();
    this.initPoetry();
}
