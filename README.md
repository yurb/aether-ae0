# aether poetry screen

## Setup

Requirements:
 - `git`
 - `npm`

```bash
git clone REPO
cd REPO
npm install
npm run build
```
